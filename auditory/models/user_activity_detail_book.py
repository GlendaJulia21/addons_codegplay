# -*- coding: utf-8 -*-

from odoo import models, fields, api

class UserActivityDetailBook(models.Model):
	_name = 'user.activity.detail.book'
	_auto = False
	
	apartment = fields.Char(string='Departamento')
	user = fields.Char(string='Usuario')
	activity = fields.Char(string='Actividad')
	last_visited = fields.Datetime(string=u'Última Visita')
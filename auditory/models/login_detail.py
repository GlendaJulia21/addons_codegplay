# -*- coding: utf-8 -*-

from odoo import models, fields, api

class LoginDetail(models.Model):
	_name = 'login.detail'

	name = fields.Char(string="Usuario")
	user_id = fields.Many2one('res.users', string='Usuario',required=True)
	datetime_login = fields.Datetime(string="Fecha y Hora de Ingreso", default=lambda self: fields.datetime.now())
	ip_address = fields.Char(string=u"Dirección IP")
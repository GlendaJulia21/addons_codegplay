# -*- coding: utf-8 -*-

from odoo import models, fields, api

class LoginDetailBook(models.Model):
	_name = 'login.detail.book'
	_auto = False
	_order = "datetime_login desc"
	
	user = fields.Char(string='Usuario')
	datetime_login = fields.Datetime(string=u'Ingreso')
# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from werkzeug.urls import url_encode


class UserActivityDetail(models.Model):
	_name = 'user.activity.detail'
	_order = "last_visited desc"

	@api.depends('model','res_id')
	def get_record_name(self):
		for activity in self:
			if activity.model and activity.res_id:
				record = self.env[activity.model].browse(activity.res_id)
				activity.name = record.name_get()[0][1]

	name = fields.Char(string='Registro', compute='get_record_name',store=True)
	model = fields.Char(string='Model')
	apartment = fields.Char(string='Departamento')
	res_id = fields.Integer(string='Res ID')
	last_visited = fields.Datetime(string=u'Última visita')
	user_id = fields.Many2one('res.users', string='Usuario')
	activity = fields.Text(string='Actividad')
	company_id = fields.Many2one('res.company', string=u'Compañía')

	@api.model
	def get_record(self, model, res_id):
		return self.env[model].sudo().browse(res_id)

	def to_record(self):
		for activity in self:
			if activity.model and activity.res_id:
				params = {
					'model': activity.model,
					'res_id': activity.res_id,
				}
				record = self.env[activity.model].browse(activity.res_id)
				return {
					'type': 'ir.actions.act_url',
					'url': '/mail/view?' + url_encode(params),
					'target': 'self',
					'target_type': 'public',
					'res_id': record.id,
				}

	@api.model
	def get_activity_detail(self, model, res_id, changes=False):
		record = self.get_record(model, res_id)
		current_time = datetime.now()
		user = self.env.user.id
		if not changes and model not in ['user.activity.detail']:
			if model in ['account.move','account.account','account.payment','account.bank.statement']:
				apartment = 'CONTABILIDAD'
			elif model in ['account.analytic.account','account.analytic.tag']:
				apartment = u'ANALÍTICA'
			elif model in ['res.partner','res.partner.bank','res.bank','res.company','res.users','res.currency','res.country','res.country.state']:
				apartment = 'BASE'
			elif model in ['crm.lead','crm.team']:
				apartment = 'CRM'
			elif model in ['product.category','product.product','product.attribute','product.template','stock.inventory','stock.location','stock.move','stock.picking','stock.warehouse']:
				apartment = u'ALMACÉN'
			elif model in ['purchase.order','purchase.order.line']:
				apartment = u'COMPRAS'
			elif model in ['sale.order','sale.order.line']:
				apartment = 'VENTAS'
			elif model in ['hr.employee','hr.contract','hr.expense','hr.payslip','hr.payslip.run']:
				apartment = 'NÓMINA'
			else:
				apartment = 'OTROS'

			self.sudo().create({'model': model,
								'res_id': res_id,
								'apartment':apartment,
								'user_id': user,
								'last_visited': current_time,
								'company_id':self.env.company.id})
		if changes:
			response_text = 'Encuentre las siguientes actividades de usuario: \n'
			recent_record = self.sudo().search([], order='id desc', limit=1)
			fields_data = record.sudo().read(changes.keys())[0]
			for key, value in fields_data.items():
				key_value = '='.join([str(key), str(value)])
				response_text += key_value + '\n'
			if response_text and recent_record:
				recent_record.activity = response_text
# -*- encoding: utf-8 -*-
{
	'name': 'Auditoria',
	'category': 'base',
	'author': 'code_gplay',
	'depends': ['base','hr','account','stock','crm','sale','purchase','popup_it','report_tools'],
	'version': '1.0',
	'description':"""
	Ver Actividad Reciente de Usuarios:
	- Ingreso al Sistema
	- Compras
	- Ventas
	- CRM
	- Kardex
	- Facturación

	Obtencion de Reportes en base a las Actividades Recientes de Usuarios:
	- PDF
	- Pantalla
	- Excel

	Grupos:
	- Usuario
	- Manager
	
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'security/security.xml',
			'security/ir.model.access.csv',
			'data/attachment_sample.xml',
            'views/user_activity_view.xml',
			'views/login_detail.xml',
			'views/menu_views.xml',
            'views/custom.xml',
			'views/user_activity_detail_book.xml',
			'views/login_detail_book.xml',
			'wizards/login_detail_wizard.xml',
			'wizards/user_activity_detail_wizard.xml',
			'wizards/import_login_detail.xml'
			],
	'installable': True,
    'application': True
}
# -*- coding: utf-8 -*-

import time
import tempfile
import binascii
import xlrd
from datetime import date, datetime, timedelta
from odoo.exceptions import Warning, UserError
from odoo.osv import osv
from odoo import models, fields, exceptions, api, _
import logging
_logger = logging.getLogger(__name__)
import io
try:
	import csv
except ImportError:
	_logger.debug('Cannot `import csv`.')
try:
	import xlwt
except ImportError:
	_logger.debug('Cannot `import xlwt`.')
try:
	import cStringIO
except ImportError:
	_logger.debug('Cannot `import cStringIO`.')
try:
	import base64
except ImportError:
	_logger.debug('Cannot `import base64`.')

class ImportLoginDetail(models.TransientModel):
	_name = 'import.login.detail'

	document_file = fields.Binary(string='Excel')
	name_file = fields.Char(string='Nombre de Archivo')

	def action_import(self):
		if not self.document_file:
			raise UserError('Tiene que cargar un archivo.')
		
		try:
			fp = tempfile.NamedTemporaryFile(delete= False,suffix=".xlsx")
			fp.write(binascii.a2b_base64(self.document_file))
			fp.seek(0)
			workbook = xlrd.open_workbook(fp.name)
			sheet = workbook.sheet_by_index(0)
		except:
			raise Warning(_("Archivo invalido!"))

		for row_no in range(sheet.nrows):
			if row_no <= 0:
				continue
			else:
				line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))
				if len(line) == 3:
					if line[1] != '':
						date_string = line[1]
						datetime_object = datetime.strptime(date_string[:19], '%Y/%m/%d %H:%M:%S')
					values = ({'user_id':line[0],
								'datetime':datetime_object + timedelta(hours=5),
								'address_ip': line[2],
								})
				elif len(line) > 3:
					raise Warning(_('Tu archivo tiene columnas mas columnas de lo esperado.'))
				else:
					raise Warning(_('Tu archivo tiene columnas menos columnas de lo esperado.'))
				
				self.make_login_detail(values)

		return self.env['popup.it'].get_message(u'SE IMPORTARON CORRECTAMENTE LOS INGRESOS AL SISTEMA.')

	def make_login_detail(self,values):
		login_obj = self.env['login.detail']

		#VALIDATIONS
		if str(values.get('user_id')) == '':
			raise Warning(_('El campo "user_id" no puede estar vacio.'))

		s = str(values.get('user_id'))
		user_id = self.find_user(s)

		login_id = login_obj.create({
			'user_id': user_id.id,
			'name': user_id.name,
			'datetime_login': values.get('datetime'),
			'ip_address': values.get('address_ip')
		})

	def find_user(self, name):
		user_obj = self.env['res.users']
		user_search = user_obj.search([('name', '=', name)],limit=1)
		if user_search:
			return user_search
		else:
			raise Warning(_('No existe un Usuario con el Nombre "%s"') % name)

	def download_template(self):
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/binary/download_template_import_login_detail',
			 'target': 'new',
			 }
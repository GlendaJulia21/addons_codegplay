# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64
import os

from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4,letter
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.utils import simpleSplit
import decimal

class UserActivityDetailWizard(models.TransientModel):
	_name = 'user.activity.detail.wizard'

	
	mode = fields.Selection([('all','Todos los Usuarios'),('one','Solo un Usuario')],string='Modo',default='all',required=True)
	user_id = fields.Many2one('res.users',string='Usuarios')
	mode_2 = fields.Selection([('all_apartments','Todos los Departamentos'),('one_apartment','Solo un Departamento')],string='Tipo',default='all_apartments',required=True)
	apartments = fields.Selection([('sale','VENTAS'),
								('purchase','COMPRAS'),
								('crm','CRM'),
								('stock',u'ALMACÉN'),
								('hr',u'NÓMINA'),
								('base','BASE'),
								('account','CONTABILIDAD'),
								('analytic','ANALÍTICA'),
								('others','OTROS')],
								string='Departamento',default='sale')
	date_ini = fields.Date(string=u'Fecha Inicial',required=True)
	date_end = fields.Date(string=u'Fecha Final',required=True)
	type_show = fields.Selection([('pantalla','Pantalla'),('excel','Excel'),('pdf','PDF')],string=u'Mostrar en', required=True, default='pantalla')
	company_id = fields.Many2one('res.company',string=u'Compañia',required=True, default=lambda self: self.env.company,readonly=True)

	def get_report(self):
		self.env.cr.execute("""CREATE OR REPLACE view user_activity_detail_book AS %s """ % (self.get_sql()))
		if self.type_show == 'pantalla':
			return {
				'name': 'Reporte Actividades Recientes',
				'type': 'ir.actions.act_window',
				'res_model': 'user.activity.detail.book',
				'view_mode': 'tree,pivot,graph',
				'view_type': 'form',
			}
		elif self.type_show == 'excel':
			return self.get_excel()
		else:
			return self.get_pdf()

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook

		ReportBase = self.env['report.base']
		workbook = Workbook(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'files','report_activity.xlsx')))
		workbook, formats = ReportBase.get_formats(workbook)

		import importlib
		import sys
		importlib.reload(sys)

		worksheet = workbook.add_worksheet("REPORTE ACT. RECIENTES")
		worksheet.set_tab_color('blue')

		worksheet.set_row(1,60)

		worksheet.merge_range(1,0,1,3, "ACTIVIDADES RECIENTE(S)", formats['especial3'])

		worksheet.write(3,0, u"Compañía:", formats['especial2'])
		worksheet.write(4,0, "RUC:", formats['especial2'])
		worksheet.write(3,2, "Fecha:", formats['especial2'])
		today = fields.Date.context_today(self)
		worksheet.write(3,1, self.company_id.name, formats['especial4'])
		worksheet.write(4,1, self.company_id.partner_id.vat if self.company_id.partner_id.vat else '', formats['especial4'])
		worksheet.write(3,3, today, formats['especialdate'])

		HEADERS = ['ACTIVIDAD','DEPARTAMENTO',u'ÚLTIMA VISITA','USUARIO']
		worksheet = ReportBase.get_headers(worksheet,HEADERS,6,0,formats['boldbord'])

		x=7

		for line in self.env['user.activity.detail.book'].search([]):
			worksheet.write(x,0,line.activity if line.activity else '',formats['especial1'])
			worksheet.write(x,1,line.apartment if line.apartment else '',formats['especial1'])
			worksheet.write(x,2,line.last_visited if line.last_visited else '',formats['datetimeformat'])
			worksheet.write(x,3,line.user if line.user else '',formats['especial1'])
			x+=1

		widths = [30,55,18,40]

		worksheet = ReportBase.resize_cells(worksheet,widths)
		workbook.close()

		f = open(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'files','report_activity.xlsx')), 'rb')
		return self.env['popup.it'].get_file('Reporte de Actividades.xlsx',base64.encodestring(b''.join(f.readlines())))

	def get_pdf(self):
		import importlib
		import sys
		importlib.reload(sys)

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal-12, "*** REPORTE DE ACTIVIDADES ***")
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-10,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-20, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-30, self.company_id.partner_id.vat if self.company_id.partner_id.vat else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=9><b>USUARIO</b></font>",style),
				Paragraph("<font size=9><b>ACTIVIDAD</b></font>",style), 
				Paragraph("<font size=9><b>ÚLTIMA VISITA</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=(20))
			t.setStyle(TableStyle([
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri')
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-85)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-95
			else:
				return pagina,posactual-valor

		width ,height  = A4  # 595 , 842
		wReal = width- 15
		hReal = height - 40

		c = canvas.Canvas(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'files','report_activity.pdf')), pagesize= A4 )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [180,250,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-50

		c.setFont("Helvetica", 11)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(self.get_sql())
		res = self.env.cr.dictfetchall()

		cont = 0
		apartment = ''
		user = ''

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 11)
			if cont == 0:
				apartment = i['apartment']
				user = i['user']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,apartment)
				pos_inicial -= 15

			if apartment != i['apartment']:
				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 11)
				apartment = i['apartment']
				user = i['user']
				c.drawString( first_pos+2 ,pos_inicial,apartment)
				pos_inicial -= 15

			if user != i['user']:
				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				user = i['user']

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['user'] if i['user'] else '',150) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['activity'] if i['activity'] else '',200) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial, i['last_visited'].strftime('%Y/%m/%d %H:%M:%S') if i['last_visited'] else '')
			first_pos += size_widths[2]

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,14,pagina,size_widths)

		c.save()

		f = open(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'files','report_activity.pdf')), 'rb')		
		return self.env['popup.it'].get_file('Reporte de Actividades.pdf',base64.encodestring(b''.join(f.readlines())))

	def get_sql(self):
		sql_user = """ AND UAD.user_id = %s"""%(str(self.user_id.id)) if self.mode == 'one' else ''
		sql_apartment = """ AND UAD.apartment = '%s'"""%(dict(self._fields['apartments'].selection).get(self.apartments)) if self.mode_2 == 'one_apartment' else ''
		
		sql = """
			SELECT 
			row_number() OVER () AS id,
			UAD.name as activity,
			UAD.apartment,
			UAD.last_visited::TIMESTAMP - '5 hr, 00 min'::INTERVAL AS last_visited,
			RP.name as user
			FROM user_activity_detail UAD
			LEFT JOIN res_users RU ON RU.id = UAD.user_id
			LEFT JOIN res_partner RP ON RP.id = RU.partner_id
			WHERE (UAD.last_visited BETWEEN '%s' AND '%s') %s %s 
			AND UAD.company_id = %s
			ORDER BY UAD.apartment, UAD.user_id
		""" % (self.date_ini.strftime('%Y/%m/%d'),
			self.date_end.strftime('%Y/%m/%d'),
			sql_apartment,
			sql_user,
			str(self.company_id.id))

		return sql

	@api.onchange('date_ini','date_end')
	def domain_dates(self):
		if self.date_ini and self.date_end:
			if self.date_end < self.date_ini:
				raise UserError("La fecha final no puede ser menor a la fecha inicial.")